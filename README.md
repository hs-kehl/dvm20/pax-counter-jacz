---
title: 'Projektdokumentation PAX-Counter Jacz'
abstract: Modul 1.7 – IT-Management" im Wintersemester 2022/2023 an der Hochschule Kehl'
author:
    - Nadim Bajwa
    - Gina Siewert
    - Jolina Seger
    - David Kuhn
    - Niklas Schell
    - Yannis Tränkner
date: \today

link-citations: true
toc: true
toc-title: Inhaltsverzeichnis
nocite: '@*'
titlepage: true
toc-own-page: true
titlepage-logo: "images/logo_HSKE.pdf"
logo-width: 130mm
---

# Einleitung

Digitalisierung hat viele Facetten. Neben einer Vielzahl weiterer technischen Neuerungen und Technologien, ist der Einsatz von Sensoren gerade in der öffentlichen Verwaltung dabei ein zunehmend wichtiger Aspekt. Mithilfe der gewonnenen Daten der Sensoren können smarte Lösungen für altbewährte Probleme, aber auch ganz neue, praktische Anwendungsmöglichkeiten gefunden werden.
Die denkbar günstigste Variante ist dabei der Einsatz von LoRaWAN-Sensoren, da diese viele praktikable Eigenschaften, mit unkomplizierter Anwedung verbinden. Als angehende Digitalisierungsexperten, war es daher naheliegend, diese Thematik im Rahmen eines Projektes in unserem Studium zu bearbeiten.   

Ziel des Projektes, das im fünften Semester angesiedelt ist, war die Einrichtung eines PAX-Counters in der beliebten Studentenkneipe Jacz. Der PAX-Counter im Jacz soll dazu dienen, die Anzahl der  anwesenden Personen zu ermitteln, um die aktuelle Auslastung im Lokal abzubilden. Der Nutzen für Besucher liegt darin, anhand eines Online-Dashboards mit farbiger Skala bereits vorab zu erkennen, ob sich ein Besuch des Lokals aktuell lohnt, also wie viele Gäste vor Ort sind oder ob dieses eventuell bereits überfüllt ist und sich der Weg somit erübrigt oder gegebenenfalls zu einem späteren Zeitpunkt sinnig ist. Für das Lokal liegt der Nutzen darin, dass an der Anzahl der Personen an den verschiedenen Wochentagen ein Trend abgelesen werden kann und demnach Öffnungszeiten, Personalbedarf oder andere Dinge angepasst werden können.

In dieser Dokumentation wird ausgehend von den technischen Grundlagen, eine Projektskizze mit allen Arbeitschritten, sowie eine genaue Spezifikation des Projektes dargestellt, um nachverfolgbar zu machen, wie die Umsetzung erfolgt ist. 


# Theoretische Grundlagen

## LoRaWAN
Kurz gesagt ist LoRaWAN eine Funktechnologie im Internet der Dinge. Jene Funktechnologie macht aktuell große Schlagzeilen in vielen Städten Deutschlands. LoRaWAN läuft auf einer Frequenz von 868 MHz und zeichnet sich durch einen geringen Energieverbrauch und eine große Reichweite aus. Allerdings ist es daher auch nur möglich, verhältnismäßig kleine Datenpakete zu versenden. Telefonanrufe sind beispielsweise nicht möglich, dafür aber die Übertragung von Sensordaten. Daraus ergeben sich die unterschiedlichsten kommunalen Anwendungsbeispiele von moderner Sensorik. Beispielsweise die intelligenten Bäume, die melden, sobald sie zu wenig Wasser haben und gegossen werden müssen oder die smarten Mülleimer, die Alarm schlagen, wenn sie voll sind. Dass diese im ersten Moment amüsant klingenden Beispiele in der Praxis einen ausschlaggebenden Nutzen haben, macht dieses Thema noch interessanter. Ein weiteres interessantes Anwendungsbeispiel auf kommunaler Ebene stellen die PAX Counter Sensoren dar, um welche sich unser Projekt drehen soll. LoRaWAN fällt unter die Kategorie ‚Low Power Wide Area Network‘ kurz LPWAN. Es ist speziell für das Internet der Dinge entwickelt (vgl. Linnemann, et al. 2019: 113). Der grundlegende Unterschied zu den bekannten Mobilfunknetzen sind die niedrigen Datenraten und der niedrige Energieverbrauch (vgl. Dartmann und Bach 2020: 3f.). Die folgende Tabelle spiegelt dies wider.

![OpenCoDe](./images/Eigenschaften_verschiedener_Funktechnologien.jpg)

## Aufbau eines LoRaWAN-Netzwerks
Der Aufbau eines LoRaWAN- Netzes sieht wie folgt aus. Wie bereits erwähnt geht es hauptsächlich um die Anwendung von Sensorik, in unserem Beispiel PAX Counter, welche über Antennen, sogenannte Gateways, mit den Servern verbunden sind. Der Einsatz verschiedenster Sensoren erleichtert die öffentliche Arbeit in vielen Bereichen erheblich und genießt deshalb aktuell einen rasanten Anstieg in der Nutzerzahl (vgl. Dartmann und Bach 2020: 7). Über Sensorik, wie beispielsweise der Pax-Counter, lassen sich die Daten der Städte zusammentragen. Die Sensorik ist mit einem LoRaWAN Netz verbunden, beispielsweise dem TTN-Netz. Hierbei gelangen die erfassten Daten durch sogenannte LoRaWAN-Gateways auf die Netzwerkserver, wo die Daten weiterverarbeitet werden, bis sie dann beispielsweise im Browser abrufbar sind. Die Gateways sind also sozusagen die Vermittler zwischen den Sensoren und den Netzwerkservern und sind daher von immenser Bedeutung.

![OpenCoDe](./images/Aufbau The Things Network.png)

## IoT
Das Internet der Dinge breitet sich immer weiter aus. Eine Funktechnologie hierbei ist LoRaWAN, was für Long Range Wide Area Network steht. Nun muss jedoch zuerst die Frage geklärt werden, was das Internet der Dinge, kurz IoT (Englisch: Internet of Things), ist. Hierzu gibt es keine allgemeingültige Definition, obwohl es mittlerweile schon vor knapp 30 Jahren das erste Mal erwähnt wurde. Viele Experten einigen sich jedoch darauf, dass das Internet der Dinge für die Verknüpfung des Internets mit der realen Welt steht (vgl. umwelt-campus.de o.J.; Horvath 2012). Im Allgemeinen bekommen Geräte eine eindeutige Identität und werden mit elektronischer Intelligenz ausgestattet. Durch das Internet of Things, IoT, werden Gegenstände eindeutig identifizierbar und können Befehle entgegennehmen. Somit sind sie in der Lage, Aufgaben voll automatisiert auszuführen, in dem sie über das Internet kommunizieren. Die technische Grundlage für das IoT bildet Mikroprozessortechnik. Da diese Prozessoren günstiger, kleiner und leistungsfähiger werden, lassen sich Gegenstände immer leichter mit elektronischer Intelligenz ausstatten. Zusätzlich zu dem Prozessor benötigt der Gegenstand noch eine kabelgebundene oder drahtlose Schnittstelle zur Anbindung an das Internet, damit Daten gesendet und empfangen werden können. Als drahtlose Übertragungstechniken kommen z.B. WLAN, Bluetooth oder wie bei uns LoRaWAN zum Einsatz. Da die Geräte durch das Internet verbunden sind, sind sie auch von der potenziellen Gefahr durch den Missbrauch oder Angriff von Dritten ausgesetzt. Damit dieses Risiko möglichst geringgehalten wird müssen gewisse Aspekte in der Sicherheitsarchitektur beachtet werden. Dazu gehören ein wirksames und zuverlässiges Identitäts- und Zugriffsmanagement, die Verschlüsselung sämtlicher Daten, der Schutz der einzelnen Systeme durch Firewalls und ein funktionierendes Software- und Patchmanagement über den gesamten Betriebszeitraum, um erkannte Fehler beheben zu können. Zur Anwendung kommt das IoT im privaten, als auch professionellen Bereich. Auf privater Seite sind das vor allem der Einsatz für die Gebäudeautomatisation. Im professionellen Umfeld wird es vor allem in der Automobilindustrie, Warenwirtschaft und dem Gesundheitswesen eingesetzt. So ist es durch intelligente Systeme möglich Transportwege oder medizinische Abläufe zu überwachen aber auch Fahrzeuge direkt mit Kfz-Werkstätten kommunizieren zu lassen (vgl. Litzel/Luber 2019).

## PAX-Counter
Das Wort PAX kommt aus der Luftfahrt. Es lässt sich von dem englischen Wort „Passangers“ ableiten, das auf Deutsch Fluggäste heißt. Es geht also darum die „Besucherzahl“ an einem Ort zu schätzen. Hierbei werden die Smartphones der Besucher registriert und die Daten über LoRaWAN an ein zentrales Datenportal weitergeleitet. Es werden WLAN- und Bluetooth-Signale der mobilen Endgeräte erkannt. Der PAX-Counter soll in der Bar „Jacz“ angebracht werden. Hierzu muss für den PAX-Counter eine Steckdose zur Verfügung gestellt werden, so dass es eine stetige Stromversorgung gibt, welche den PAX-Counter mit Energie versorgt. Bei dem Tracking von mobilen Endgeräten kommen bei den meisten Bedenken auf, ob dieses mit dem Datenschutz vereinbar ist. Jedoch kann aus datenschutzrechtlicher Sicht Entwarnung gegeben werden. Der Einsatz von PAX-Counter ist vollkommen datenschutzkonform. Es geht hierbei lediglich um die Anzahl von Mobilgeräten zu einem bestimmten Zeitpunkt an einem bestimmten Ort. Es werden keine einzelnen Signale verfolgt. Weder wird beobachtet, wohin sich diese bewegen, noch wie lange sich diese an einem Ort befinden.
Jedoch stellt sich die Frage, wie die reale Besucheranzahl festgestellt werden kann, wenn die PAX-Counter nur die mobilen Endgeräte zählen? Denn zweifelsohne wird es Gäste geben, die ohne mobiles Endgerät erscheinen und im Gegensatz dazu auch einige, die neben ihrem Smartphone noch eine Smart-Watch dabei haben, welche auch gezählt würde. Des Weiteren gibt es auch über der Bar noch Wohnungen, in denen es mit großer Wahrscheinlichkeit mehrere Geräte gibt, die der PAX-Counter erfassen würde.
Um diesem Problem entgegen zu wirken sollte an unterschiedlichen Tagen, über längere Zeiträume die tatsächliche Besucherzahl vor Ort gemessen und mit der ausgegebenen Zahl des Sensors verglichen werden. Wenn das mehrmals stattgefunden hat, kann man gewisse Relationen herstellen, die dann mit großer Wahrscheinlichkeit zutreffen. Es muss ein Schlüssel entwickelt werden, um die dann letztlich in der App ausgegebenen Zahlen so realistisch wie möglich zu gestalten.

# Projektskizze

Zu Beginn des Projekts wurde zunächst durch Brainstorming ermittelt, wie die Sensoren sinnvoll eingesetzt werden können. Anschließend wurde gemeinschaftlich entschieden, dass wir einen PAX-Sensor zur Messung der Besucherzahlen in einer beliebten Kneipe in Kehl installieren möchten.

![OpenCoDe](./images/1.JPG)
Im ersten Schritt wurden die Bestandteile des Senosors dargestellt und das Zusammenführen dieser vorbereitet. Die Bauteile wurden an die Platine durch die Gruppenmitglieder gelötet und anschließend auf Fehler überprüft. Wichtig war dabei genau und vorsichtig zu arbeiten, da die Platine nicht beschädigt werden durfte.

![OpenCoDe](./images/2.JPG)
In einem ersten Test funktionierte der Sensor und es waren keinerlei Fehler in der Lötarbeit zu erkennen.

![OpenCoDe](./images/4.JPG)
Nach dem Löten aller Bestandteile des Sensors sah dieser, wie im obrigen Bild aus. Die Platine hat nun alle wichtigen Bestandteile, um die Funtionsfähigkeit des Sensors zu gewährleisten.

![OpenCoDe](./images/5.JPG)
Der fertige Sensor wurde dann in einer grauen Mureva Box angebracht, um den Sensor vor äußerlichen Auswirkungen zu schützen und das Anbringen zu erleichtern.

![OpenCoDe](./ardublock klein.png)
Im nächsten Schritt, wurde wie im Screenshot zu sehen, durch Ardublock, die jeweiligen technischen Bauteile ermittelt. Dabei war die Feldstärke das entscheidende Kriterium, da gerade dieser Faktor darüber entscheidet, in welchem Umfang der Sensor die Daten auslesen kann. Dabei wurden die Funktionen des Sensors eingestellt, unter anderem auch die Abfragezeit bzw. Sendezeit.
Gerade die Feldstärke musste immer wieder erneut angepasst werden, da bei den Tests einige Unregelmäßigkeiten aufgetreten sind. Durch experimentieren wurde dann festgestellt, dass die Feldstärke für den PAX Counter und seiner Nutzungsfähigkeit bei -95 am effektivsten ist.

![OpenCoDe](./Jacz Dokumentation.jpeg)
Nach dem erfolgreichen Tests in unserem Klassenzimmer wurde der Sensor, wie für sein Nutzen angedacht, in der Studentenkneipe Jacz angebracht. Auch dort wurden die Daten erneut überprüft und der Sensor an einer geeigneten Stelle angebracht.

![OpenCoDe](Darstellungsform der Daten.jpeg)
Um die Daten gut lesbar zu machen, haben wir uns dafür entschieden, eine Grafik zu verwenden, die sowohl ansprechend ist, aber auch die Besucherzahl gut sichtbar macht. Der Benutzer sieht visuell auf einen Blick, ob das Lokal überfüllt ist oder nicht. Der Rote Bereich signalisiert also, dass ein Besuch nicht empfehlenwert wäre.

## Anforderungen und Herausforderungen

Im Rahmen unseres Projekts haben wir uns für einen Anwendungsfall entschieden, der den Studenten der Hochschule Kehl einen echten Mehrwert liefern soll. Eine Lösung von Studenten für Studenten.
Die Idee, einen Paxcounter im Jacz anzubringen, entstand recht schnell. Um diese jedoch umsetzen zu können, mussten wir dort zunächst entsprechende Überzeugungsarbeit leisten. Folglich wollten wir unseren Anwendungsfall so anpassen, dass er nicht nur den Kehler Studenten, sondern auch den Mitarbeitern und Besitzer des Jacz in Zukunft einen Mehrwert bietet. 

Die Datenauswertung des Paxcounters lässt sich in zwei Kategorien gliedern. Auf der einen Seite haben wir die jeweils aktuellen Besucherzahlen im Jacz, auf der anderen Seite lässt sich rückblickend beobachten, wie sich eben diese Zahlen über die letzten Tage und Wochen verhalten haben. 

Für das Jacz ist nun vor allem der zweite Fall interessant. Über die Zeit soll auf unserem Dashboard eine Graphik entstehen, welche verbildlicht, an welchen Tagen bzw. zu welchen Zeiten im Jacz mehr oder auch weniger Betrieb ist. Sofern sich hierbei ein Muster erkennen lässt, können die Öffnungszeiten und die Zahl der Mitarbeiter im Jacz zukünftig entsprechend angepasst werden. Das heißt an Tagen, an denen in der Regel weniger oder kaum Betrieb ist, kann das Jacz später / in kleinerem Rahmen oder gegebenenfalls auch gar nicht öffnen bzw. früher schließen. So kann unnötiger Arbeitsaufwand eingespart werden.
Anschließend wurde erklärt, dass mit unserer Technologie keine Daten gesammelt werden, die ohne weiteres auf eine bestimmte Person zurückgeführt werden können, man sich hier also nicht sorgen müsse.
Schließlich konnten wir das Jacz von unserer Anwendung überzeugen und das Projekt konnte weiter angepasst werden.

Der zweite Fall, der uns persönlich besonders wichtig war, ist der, der den Kehler Studenten einen Mehrwert liefern soll. Diese werden sich weniger über den zeitlichen Verlauf der Besucherzahlen im Jacz interessieren, sondern viel mehr für den aktuellen Stand. Wir brauchten also eine Darstellungsform, welche verbildlicht, ob sich ein Besuch zum jeweiligen Zeitpunkt lohnt - also ob gerade sehr wenig, bereits zu viel oder ein sehr angenehmer Betrieb herrscht. Unser Ziel war dies auf einen Blick ersichtlich zu machen, weshalb wir eine Graphik entwickeln wollten, welche die Besucherzahlen in einen grünen, gelben und roten Bereich gliedert, je nach Betrieb. Außerdem sollte ebenfalls die tatsächliche Zahl der Besucher auf derselben Graphik angezeigt werden, um das Ergebnis möglichst minimalistisch und praktikabel zu halten. Zudem sollte die Graphik möglichst ansprechend gestaltet werden, da sie später auch tatsächlich von den Studenten genutzt werden soll.
Wir entschieden uns daher für die Vorlage eines Tachos und haben diese entsprechend unseren Bedürfnissen angepasst, um unser Ziel zu erreichen.

Neben den konzeptionellen Anforderungen stellte sich vor allem die Anpassung des Sensors als Herausforderung dar. 
Zunächst stellte sich die Frage, wie hoch die Feldstärke sein muss, damit der Sensor nutzbare Daten liefert. Hier mussten wir eine Zeit lang in der Hochschule testen und ausprobieren, bis wir eine Feldstärke fanden, mit der realistische Werte geliefert wurden.
Außerdem mussten die Werte zusätzlich angepasst werden, da teilweise mehrere Geräte pro Student oder Geräte aus nebenliegenden Räumen Signale senden, welche das Ergebnis verfälschen. Zu Beginn waren die Daten auch sehr unregelmäßig, sodass die Anpassung eine gewisse Zeit dauerte. Nachdem diese in der Hochschule jedoch erfolgreich durchgeführt wurde, musste derselbe Prozess noch einmal nach der Anbringung des Sensors im Jacz durchgeführt werden. Auch hier gibt es umliegende Räumlichkeiten, die das Ergebnis beeinflussen können und die Feldstärke musste erneut getestet werden.

Das viel größere Problem hier war jedoch, dass der Sensor, nachdem er im Jacz angebracht wurde, zunächst keine Daten mehr an das Dashboard übertragen hat. Die Ursache konnte jedoch recht schnell gefunden werden, da schlicht die Reichweite der Antenne nicht ausreichend war. Dies sollte anschließend behoben werden, durch eine passendere Position der Antenne. 



## Zukunfstsausblick

Auch nach Abschluss des Projekts, sollen sowohl die Kehler Studenten als auch das Jacz weiterhin Zugriff auf das Dashboard zu unserem Paxcounter erhalten. 

Für das Jacz macht das ganze nur dann wirklich Sinn, wenn es sich kontinuierlich an die Begebenheiten anpassen kann und nicht nur ein einziges Mal.

Für die Studenten wäre eine Einbindung des Dashboards in der Hochschul-App denkbar und wünschenswert. Schließlich soll die Übersicht leicht auffindbar sein, damit sie praktisch genutzt werden kann. 

Ein Anwendungsfall wie er hier entwickelt wurde, lässt sich sicherlich auf viele andere Situationen übertragen. Überall dort wo jegliche Anpassungen an Besucherzahlen Sinn machen, kann solch ein Paxcounter von Vorteil sein. Nachdem ein solcher Fall einmal erfolgreich getestet wurde, kann er anschließend als "Vorlage" oder "Muster" für andere Fälle dienen.



# Literaturverzeichnis

Dartmann, G. & Bach, C., 2020. Offene IoT-Funknetze und offene Daten für eine "Open Region" am Beispiel der Region Stuttgart. Positionspapier der Expertengruppe Internet der Dinge, s.l.: s.n.

Horvath, S., 2012. bundestag.de. [Online]
Available at:
https://www.bundestag.de/resource/blob/192512/cfa9e76cdcf46f34a941298efa7e8
5c9/internet_der_dinge-data.pdf
[Zugriff am 10 Januar 2023].

Linnemann, M., Sommer, A. & Leufkes, R., 2019. Einsatzpotentiale von
LoRaWAN in der Energiewirtschaft. Praxisbuch zu Technik, Anwendung und
regulatorischen Randbedingungen.. Wiesbaden: Springer Vieweg.

Litzel, Nico/Stefan Luber (2019): Was ist das Internet of Things?, BigData-Insider, [online] https://www.bigdata-insider.de/was-ist-das-internet-of-things-a-590806/.

umwelt-campus.de, kein Datum Umwelt-campus.de. [Online]
Available at: https://www.umwelt-campus.de/iotwerkstatt/tutorials/mitmachklima-lorawan-als-offenes-netz-zur-daseinsvorsorge
[Zugriff am 12 Januar 2023]


